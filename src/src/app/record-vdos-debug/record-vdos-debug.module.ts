import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import { RecordVdosDebugRoutes } from './record-vdos-debug.routing';
import { RecordVdosDebugComponent } from './record-vdos-debug.component';

import {CalendarModule} from 'primeng/calendar';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(RecordVdosDebugRoutes),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    CalendarModule,

  ],
  declarations: [RecordVdosDebugComponent]
})
export class RecordVdosDebugModule { }
