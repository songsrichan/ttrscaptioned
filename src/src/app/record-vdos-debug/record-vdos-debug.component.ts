import { Component, OnInit } from '@angular/core';
export interface Food {
  value: string;
  viewValue: string;
}
export interface Food2 {
  value: string;
  viewValue: string;
}
declare const $: any;
@Component({
  selector: 'app-record-vdos-debug',
  templateUrl: './record-vdos-debug.component.html',
  styleUrls: ['./record-vdos-debug.component.css']
})
export class RecordVdosDebugComponent implements OnInit {
  date8: Date;
  isValid = true;
  isValidvri = true;
  isValidtoggle = true;

  foods: Food[] = [
    { value: 'steak-0', viewValue: 'Steak' },
    { value: 'pizza-1', viewValue: 'Pizza' },
    { value: 'tacos-2', viewValue: 'Tacos' }
  ];

  foods2: Food2[] = [
    { value: 'ddd-2', viewValue: 'wwa' },
    { value: 'sssss-3', viewValue: 'aaaa' },
    { value: 'wwww-4', viewValue: 'dddd' }
  ];
  constructor() { }

  ngOnInit() {

  }
  showNotification(from: any, align: any) {
    const type = ['', 'info', 'success', 'warning', 'danger', 'rose', 'primary'];

    const color = Math.floor((Math.random() * 6) + 1);

    $.notify({
        icon: 'perm_phone_msg',
        message: 'สายเรียกเข้า...'
    }, {
        type: type[color],
        timer: 3000,
        placement: {
            from: from,
            align: align
        },
        template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0} alert-with-icon" role="alert">' +
          '<button mat-raised-button type="button" aria-hidden="true" class="close" data-notify="dismiss">  <i class="material-icons">close</i></button>' +
          '<i class="material-icons" data-notify="icon">perm_phone_msg</i> ' +
          '<span data-notify="title">{1}</span> ' +
          '<span data-notify="message">{2}</span>' +
          '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:200px;"></div>' +
          '</div>' +
          '<a href="{3}" target="{4}" data-notify="url"></a>' +
        '</div>'
    });
}

  changValue(valid: true) {
    this.isValid = valid;
  }
  changValueVRI(valid: true) {
    this.isValidvri = valid;
  }
  changValuetoggle(valid: true) {
    this.isValidtoggle = valid;
  }
  btnsave() {

  }
  btncancel() {

  }


}
