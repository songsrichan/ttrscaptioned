import { Routes } from '@angular/router';

import { RecordVdosDebugComponent } from './record-vdos-debug.component';

export const RecordVdosDebugRoutes: Routes = [
    {

      path: '',
      children: [ {
        path: '',
        component: RecordVdosDebugComponent
    }]
}
];
