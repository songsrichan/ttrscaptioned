import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import { SearchRoutes } from './search.routing';
import { SearchComponent } from './search.component';

import {CalendarModule} from 'primeng/calendar';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SearchRoutes),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    CalendarModule,

  ],
  declarations: [SearchComponent]
})
export class SearchModule { }
