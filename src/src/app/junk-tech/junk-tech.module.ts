import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import { JunkTechRoutes } from './junk-tech.routing';
import { JunkTechComponent } from './junk-tech.component';

import {CalendarModule} from 'primeng/calendar';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(JunkTechRoutes),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    CalendarModule
  ],
  declarations: [JunkTechComponent]
})
export class JunkTechModule { }
