import { Routes } from '@angular/router';

import { JunkTechComponent } from './junk-tech.component';

export const JunkTechRoutes: Routes = [
    {
      path: '',
      children: [ {
        path: 'junk-tech',
        component: JunkTechComponent
    }]
}
];
