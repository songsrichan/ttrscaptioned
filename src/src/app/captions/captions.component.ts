import { Component, OnInit } from '@angular/core';

export interface Food {
  value: string;
  viewValue: string;
}
export interface Food2 {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-captions',
  templateUrl: './captions.component.html',
  styleUrls: ['./captions.component.css']
})

export class CaptionsComponent implements OnInit {
  isValid = true;
  private history ;
  foods: Food[] = [
    {value: 'steak-0', viewValue: 'Steak'},
    {value: 'pizza-1', viewValue: 'Pizza'},
    {value: 'tacos-2', viewValue: 'Tacos'}
  ];

  foods2: Food2[] = [
    {value: 'ddd-2', viewValue: 'wwa'},
    {value: 'sssss-3', viewValue: 'aaaa'},
    {value: 'wwww-4', viewValue: 'dddd'}
  ];
  constructor() { }

  ngOnInit() {
    this.history = ['2016-06-10   11:01:58   สวัสดีครับ', '2016-06-10   11:12:58   มีไรให้ช่วยครับ'
    , '2016-06-10   11:12:58   มีไรให้ช่วยครับปปปปปปปปปปปปปปปปปปปปปปปปปปป', '2016-06-10   11:12:58   มีไรให้ช่วยครับ'
    , '2016-06-10   11:12:58   มีไรให้ช่วยครับ', '2016-06-10   11:12:58   มีไรให้ช่วยครับ'
    , '2016-06-10   11:12:58   มีไรให้ช่วยครับ', '2016-06-10   11:12:58   มีไรให้ช่วยครับ']
  }

  changValue(valid: true) {
    this.isValid = valid;
  }

  btnsave() {

  }
  btncancel() {

  }
}
