import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import { CaptionsRoutes } from './captions.routing';
import { CaptionsComponent } from './captions.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CaptionsRoutes),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule
  ],
  declarations: [CaptionsComponent]
})
export class CaptionsModule { }
