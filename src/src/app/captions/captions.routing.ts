import { Routes } from '@angular/router';

import { CaptionsComponent } from './captions.component';

export const CaptionsRoutes: Routes = [
    {

      path: '',
      children: [ {
        path: '',
        component: CaptionsComponent
    }]
}
];
