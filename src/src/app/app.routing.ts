import { Routes } from '@angular/router';
// import { } from './queues/queues.module'
import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
import { AuthGuardService } from './services/auth-guard.service';
export const AppRoutes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  }, {
    path: 'callback',
    redirectTo: 'dashboard',
  }, {
    path: '',
    component: AdminLayoutComponent,
    canActivate: [AuthGuardService],
    children: [
      {
        path: '',
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'captions',
        loadChildren: './captions/captions.module#CaptionsModule'
      }, {
        path: 'stat-supervisors',
        loadChildren: './supervisor/supervisor.module#SupervisorModule'
      }, {
        path: 'search',
        loadChildren: './search/search.module#SearchModule'
      },  {
        path: 'record-vdos-debug',
        loadChildren: './record-vdos-debug/record-vdos-debug.module#RecordVdosDebugModule'
      }, {
        path: 'search-techproblems',
        loadChildren: './search-techproblems/search-techproblems.module#SearchTechproblemsModule'
      },  {
        path: 'search-vdos-debug',
        loadChildren: './search-vdos-debug/search-vdos-debug.module#SearchVdosDebugModule'
      }, {
        path: 'junk-vrsvdo',
        loadChildren: './data-delete/data-delete.module#DataDeleteModule'
      }, {
        path: 'junk-vrsvdo',
        loadChildren: './junk-tech/junk-tech.module#JunkTechModule'
      }, {
        path: 'components',
        loadChildren: './components/components.module#ComponentsModule'
      }, {
        path: 'forms',
        loadChildren: './forms/forms.module#Forms'
      }, {
        path: 'tables',
        loadChildren: './tables/tables.module#TablesModule'
      }, {
        path: 'maps',
        loadChildren: './maps/maps.module#MapsModule'
      }, {
        path: 'widgets',
        loadChildren: './widgets/widgets.module#WidgetsModule'
      }, {
        path: 'charts',
        loadChildren: './charts/charts.module#ChartsModule'
      }, {
        path: 'calendar',
        loadChildren: './calendar/calendar.module#CalendarModule'
      }, {
        path: '',
        loadChildren: './userpage/user.module#UserModule'
      }, {
        path: '',
        loadChildren: './timeline/timeline.module#TimelineModule'
      }
    ]
  }, {
    path: '',
    component: AuthLayoutComponent,
    children: [{
      path: '',
      loadChildren: './pages/pages.module#PagesModule'
    },
    ]
  }, {
    path: '',
    component: AuthLayoutComponent,
    children: [{
      path: 'pages',
      loadChildren: './pages/pages.module#PagesModule'
    },
    ]
  }
];
