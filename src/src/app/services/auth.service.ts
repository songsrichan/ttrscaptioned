import { Injectable, EventEmitter } from '@angular/core';
import { OAuthService, NullValidationHandler, OAuthEvent } from 'angular-oauth2-oidc';
import { authConfig } from '../auth.config';
import { Router } from '@angular/router';
@Injectable()
export class AuthService {
  constructor(private oauthService: OAuthService, public router: Router) {
      this.oauthService.oidc = true;
      this.oauthService.configure(authConfig);
      this.oauthService.setStorage(localStorage);
      this.oauthService.tokenValidationHandler = new NullValidationHandler();
      this.oauthService.loadDiscoveryDocument('');
      this.oauthService.tryLogin();
  }

  login() {
    this.oauthService.initImplicitFlow();
  }

  loginWithPassword(userName, password) {
    this.oauthService
      .fetchTokenUsingPasswordFlowAndLoadUserProfile(
        userName,
        password
      )
      .then(() => {
        console.log('successfully logged in');
        // this.loginFailed = false;
      })
      .catch(err => {
        console.error('error logging in', err);
        // this.loginFailed = true;
      });
  }

  logout() {
    this.oauthService.logOut();
  }
  public hasValidIdToken() {
    return this.oauthService.hasValidIdToken();
  }

  public requestAccessToken() {
    return this.oauthService.requestAccessToken;
  }

  public id_token() {
    return this.oauthService.getIdToken();
  }

  public access_token() {
    return this.oauthService.getAccessToken();
  }

  public id_token_expiration() {
    return this.oauthService.getIdTokenExpiration();
  }

  public access_token_expiration() {
    return this.oauthService.getAccessTokenExpiration();
  }

  public getClaims() {
    const cliams = this.oauthService.getIdentityClaims();
    return cliams;
  }
  public scopes() {
    const scopes = this.oauthService.getGrantedScopes();
    return scopes;
  }
}
