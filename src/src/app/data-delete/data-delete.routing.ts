import { Routes } from '@angular/router';

import { DataDeleteComponent } from './data-delete.component';

export const DataDeleteRoutes: Routes = [
    {

      path: '',
      children: [ {
        path: 'delete-vp',
        component: DataDeleteComponent
    }]
}
];
