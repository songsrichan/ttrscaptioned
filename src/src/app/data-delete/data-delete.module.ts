import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import { DataDeleteRoutes } from './data-delete.routing';
import { DataDeleteComponent } from './data-delete.component';

import {CalendarModule} from 'primeng/calendar';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DataDeleteRoutes),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    CalendarModule
  ],
  declarations: [DataDeleteComponent]
})
export class DataDeleteModule { }
