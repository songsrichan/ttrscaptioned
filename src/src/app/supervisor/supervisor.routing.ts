import { Routes } from '@angular/router';

import { SupervisorComponent } from './supervisor.component';

export const SupervisorRoutes: Routes = [
    {

      path: '',
      children: [ {
        path: '',
        component: SupervisorComponent
    }]
}
];
