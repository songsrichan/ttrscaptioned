import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import { SupervisorRoutes } from './supervisor.routing';
import { SupervisorComponent } from './supervisor.component';
import {CalendarModule} from 'primeng/calendar';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SupervisorRoutes),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    CalendarModule,

  ],
  declarations: [SupervisorComponent]
})
export class SupervisorModule { }
