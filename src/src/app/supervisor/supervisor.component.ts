import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
declare const $: any;
@Component({
  selector: 'app-supervisor',
  templateUrl: './supervisor.component.html',
  styleUrls: ['./supervisor.component.css']
})


export class SupervisorComponent implements OnInit {
  date = new FormControl(new Date());
  serializedDate = new FormControl((new Date()).toISOString());
  date8: Date;
  private supervisor: any[]
  private Interpreter: any[]
  constructor() { }

  ngOnInit() {
    this.supervisor = ['จิดาภา พักศาลา', 'ภัทราภรณ์ แสนคำปา', 'วริศรา ทิพย์ชัยโรจน์', 'อัมรินทร์ อัมพร', 'เบญจมาศ สุวรรณ']
    this.Interpreter = ['นายWuttipong Kumwilaisak', 'นายณรงค์ฤทธิ์ แซ่จิว', 'นางสาวคมคิด ศันสนะเกียรติ', 'นางสาวอัฑฒกาฐ์ เที่ยงปรีชารักษ์',
      'นายกนต์ธร ไกรสุวรรณ', 'นายคเชนทร์ เทพทับทิม', 'นางสาวชนัญชิดา ชีพเสรี', 'นายภัทรพงศ์ พิกุลเงิน',
      'นางสาวฐิตารีย์ เกี่ยวคุ้มภัย', 'นายจารุพัฒน์ หิรัญศรีตระกูล', 'นางสาวชวาลา สวัสดิ์สว่าง', 'นางสาวดวงฤดี แสนสุข',
      'นางสาวพจี เอี่ยมขำ', ' นางสาวฤดี พานิช', 'นายนพพล ไกยราช', 'นางสาววัชรา อสิพงษ์',
      'นางสาวพัชรี ศรีสุขใส', 'นางสาวกนกอร วงษ์คำหาญ', 'นางสาวพัชรียา โพธิ์เย็น', 'นางสาวพจีรัตน์ ศรีเสมอ']

  }


}
