import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import { SearchTechproblemsRoutes } from './search-techproblems.routing';
import { SearchTechproblemsComponent } from './search-techproblems.component';

import {CalendarModule} from 'primeng/calendar';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SearchTechproblemsRoutes),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    CalendarModule
  ],
  declarations: [SearchTechproblemsComponent]
})
export class SearchTechproblemsModule { }
