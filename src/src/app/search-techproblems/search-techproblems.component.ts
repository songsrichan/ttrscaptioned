import { Component, OnInit, AfterViewInit } from '@angular/core';
declare interface DataTable {
  headerRow: string[];
  dataRows: string[][];
}
declare const $: any;
@Component({
  selector: 'app-search-techproblems',
  templateUrl: './search-techproblems.component.html',
  styleUrls: ['./search-techproblems.component.css']
})
export class SearchTechproblemsComponent implements OnInit, AfterViewInit {
  public dataTable: DataTable;
  private category;
  private contact;
  constructor() { }

  ngOnInit() {
    this.category = [['ปัญหาจากอินเทอร์เน็ต-คนหูหนวก'], ['ปัญหาจากอินเทอร์เน็ต - TTRS'], ['จอดำ'], ['Invalid Token']]
    this.contact = [['VRS Video Phone'], ['VRS Mobile'], ['VRS Kiosk'], ['VRS Web']]
    this.dataTable = {
      headerRow: ['เคส', 'วันที่', 'หัวข้อปัญหา', 'วิธีแก้ปัญหา', 'หมายเลขห้อง', 'ช่องทาง', 'หมวดหมู่', 'ผู้บันทึก', 'การจัดการ'],
      dataRows: [
        ['1900', '2018-11-07 17:20:03', 'หัวข้อปัญหา', 'วิธีการแก้ไข', '6', 'VRS Kiosk', 'จอดำ', 'admin', ''],
        ['1900', '2018-11-07 17:20:03', 'หัวข้อปัญหา', 'วิธีการแก้ไข', '6', 'VRS Kiosk', 'จอดำ', 'admin', ''],
        ['1900', '2018-11-07 17:20:03', 'หัวข้อปัญหา', 'วิธีการแก้ไข', '6', 'VRS Kiosk', 'จอดำ', 'admin', ''],
        ['1900', '2018-11-07 17:20:03', 'หัวข้อปัญหา', 'วิธีการแก้ไข', '6', 'VRS Kiosk', 'จอดำ', 'admin', ''],
        ['1900', '2018-11-07 17:20:03', 'หัวข้อปัญหา', 'วิธีการแก้ไข', '6', 'VRS Kiosk', 'จอดำ', 'admin', ''],
        ['1900', '2018-11-07 17:20:03', 'หัวข้อปัญหา', 'วิธีการแก้ไข', '6', 'VRS Kiosk', 'จอดำ', 'admin', ''],
      ]
    };
  }
  ngAfterViewInit() {
    // $('#datatables').DataTable({
    //   'pagingType': 'full_numbers',
    //   'lengthMenu': [
    //     [10, 25, 50, -1],
    //     [10, 25, 50, 'All']
    //   ],
    //   responsive: true,
    //   language: {
    //     search: '_INPUT_',
    //     searchPlaceholder: 'Search records',
    //   }

    // });

    const table = $('#datatables').DataTable();

    // Edit record
    table.on('click', '.edit', function () {
      const $tr = $(this).closest('tr');

      const data = table.row($tr).data();
      alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
    });

    // Delete a record
    table.on('click', '.remove', function (e) {
      const $tr = $(this).closest('tr');
      table.row($tr).remove().draw();
      e.preventDefault();
    });

    // Like record

    $('.card .material-datatables label').addClass('form-group');
  }

}
