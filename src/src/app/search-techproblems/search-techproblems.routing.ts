import { Routes } from '@angular/router';

import { SearchTechproblemsComponent } from './search-techproblems.component';

export const SearchTechproblemsRoutes: Routes = [
    {
      path: '',
      children: [ {
        path: '',
        component: SearchTechproblemsComponent
    }]
}
];
