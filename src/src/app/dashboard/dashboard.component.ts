import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import * as screenfull from 'screenfull';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/map';
import { Subscription } from 'rxjs/Subscription';
import { environment } from '../../environments/environment';
declare const $: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit {
  ser = 'sms';
  service1 = ['SMS', 'Message', 'Text Chat', 'Captioned', 'VP', 'Video']
  services = 'SMS';
  dashboard: any[] = [];
  dataservice: any[] = [];
  ojb: any[] = [];
  infor = [];
  group = [];
  datametric: any[] = [];
  keyarray = [];
  dataojb: any[] = [];
  subscription: Subscription;

  private Url = 'http://localhost/dashboard.php';

  constructor(private http: HttpClient, private _http: Http) { }

  public ngOnInit() {
    //  Observable.interval(3000).subscribe(x => { // will execute every 30 seconds
    this.getDataDashboard();
    //   });

    const el = document.getElementById('target');
    document.getElementById('button').addEventListener('click', () => {
      if (screenfull.enabled) {
        screenfull.request(el);
      }
    });
  }

  getDataDashboard() {
    const timestamp = Math.round(+new Date() / 1000);
    let url = environment.apiDashboardUrl + '?query={job=%22ttrsdashboard%22}&time=' + timestamp;
    url = 'http://localhost/dashboard.php';
    this.http.get(url).subscribe(
      data => {
        // const servicedashboard = data['data'].result.map((value, index) => {
          // const metric = data['data'].result[index].metric
          // const substring = metric['__name__'];
          // const name1 = substring.split('_');
          // const name = substring.split(/_(.+)/)[1];
          // console.log(name);
          // var a[metric.service] = data['data'].result[index].value[1];

          // ใช้ได้
          // this.dashboard[name1[0]][name][metric.service] = data['data'].result[index].value[1];

          // this.infor.push(name);
          // console.log(this.infor)

          // this.ojb[name1[0].name.metric['service']] = data['data'].result[index].value[1];

          // this.keyarray.push(name1[0] + name + metric['service']);

          // return data['data'].result[index].metric
          // this.datametric.push(ojb);
          // console.log(ojb[name1[0].infor])

        // })
        // console.log(this.dashboard);

        const dashboard = [];
        data['data'].result.forEach((value, index) => {
          dashboard[data['data'].result[index].metric.__name__ + '_' + data['data'].result[index].metric.service]
            = data['data'].result[index].value[1];
        })

        this.dashboard = dashboard;
      }
      ,
      err => {
        console.log('error');
      }
    );
  }
  secToTime(sec) {
    const sec_num = parseInt(sec, 10); // don't forget the second param
    const hours: any = Math.floor(sec_num / 3600);
    const minutes: any = Math.floor((sec_num - (hours * 3600)) / 60);
    const seconds: any = sec_num - (hours * 3600) - (minutes * 60);
    return hours.toString().padStart(2, '0') + ':' + minutes.toString().padStart(2, '0')
      + ':' + seconds.toString().padStart(2, '0');
    // return sec;
  }
}
