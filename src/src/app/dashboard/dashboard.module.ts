import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MdModule } from '../md/md.module';
import { MaterialModule } from '../app.module';
import { BigScreenModule } from 'angular-bigscreen';
import { NgMarqueeModule } from 'ng-marquee';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutes } from './dashboard.routing';


@NgModule({
    imports: [
        NgMarqueeModule,
        CommonModule,
        RouterModule.forChild(DashboardRoutes),
        FormsModule,
        MdModule,
        MaterialModule,
        BigScreenModule.forRoot()
    ],
    declarations: [DashboardComponent],
    providers : [  ]
})

export class DashboardModule {}
