import { Routes } from '@angular/router';

import { SearchVdosDebugComponent } from './search-vdos-debug.component';

export const SearchVdosDebugRoutes: Routes = [
    {

      path: '',
      children: [ {
        path: '',
        component: SearchVdosDebugComponent
    }]
}
];
