import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import { SearchVdosDebugRoutes } from './search-vdos-debug.routing';
import { SearchVdosDebugComponent } from './search-vdos-debug.component';

import {CalendarModule} from 'primeng/calendar';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SearchVdosDebugRoutes),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    CalendarModule,

  ],
  declarations: [SearchVdosDebugComponent]
})
export class SearchVdosDebugModule { }
